#!/bin/bash
# Cloud Init Script used to provision WSO2AM Instance on EC2 Environment
# version 1.0
# @Author - Agasthi Kothurkar
# Script will work Amazon Linux / CentOS / Fedora / Redhat distributions

#Script Variables
WSO2AM_VERSION=1.10.0
WSO2AM_PACKAGE_URL=https://s3-us-west-1.amazonaws.com/stargate.cfs/wso2am-1.10.0.zip
JDK_PACKAGE_URL=https://s3-us-west-1.amazonaws.com/stargate.cfs/jdk-8u131-linux-x64.rpm
WSO2AM_USER=wso2am
WSO2AM_INSTALL_DIR=/opt
WSO2AM_INIT_SCRIPT=https://s3-us-west-1.amazonaws.com/stargate.cfs/wso2am.sh

#Create WSO2AM User
groupadd $WSO2AM_USER
useradd -g $WSO2AM_USER -s /bin/bash -d /home/$WSO2AM_USER $WSO2AM_USER

#Install JDK. As this is an rpm install there is no need to configure the alternatives.
rpm -i $JDK_PACKAGE_URL

#Create Installation Directory
mkdir -p $WSO2AM_INSTALL_DIR

#Download package 
cd $WSO2AM_INSTALL_DIR
wget $WSO2AM_PACKAGE_URL
ln -s wso2am-$WSO2AM_VERSION wso2am

#Download init script
wget -o /etc/init.d/wso2 $WSO2AM_INIT_SCRIPT
chkconfig wso2am on

#Set permissions
chown -R $WSO2AM_USER:$WSO2AM_USER $WSO2AM_INSTALL_DIR/wso2am

#Start the WSO2AM service
service wso2am start