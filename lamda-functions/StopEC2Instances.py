import boto3

# Enter the region your instances are in, e.g. 'us-east-1'

region = 'us-west-1'

# Enter your instances here: ex. ['X-XXXXXXXX', 'X-XXXXXXXX']

instances = ['i-02ff49c8195ee35ec' , 'i-0654344b9025b490f' , 'i-086586be088867f9b' , 'i-0968b3795794b1e05']

 

def lambda_handler(event, context):

    ec2 = boto3.client('ec2', region_name=region)

    ec2.stop_instances(InstanceIds=instances)

    print 'stopped your instances: ' + str(instances)